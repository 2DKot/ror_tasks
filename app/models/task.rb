class Task < ApplicationRecord
  default_scope { order(id: :asc) }
end
