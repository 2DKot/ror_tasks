class TaskSerializer < ActiveModel::Serializer
  attributes :id, :text, :url, :completed, :completed_url

  delegate :url_helpers, to: 'Rails.application.routes'

  def url
    url_helpers.task_path(object)
  end

  def completed_url
    url_helpers.completed_status_task_path(object)
  end
end
