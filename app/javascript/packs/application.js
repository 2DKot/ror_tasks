import Vue from 'vue'
import App from '../components/app.vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)

document.addEventListener('DOMContentLoaded', () => {
  const el = document.body.appendChild(document.createElement('application'))
  const app = new Vue({
    el,
    render: h => h(App),
  })
  console.log(app)
})

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})
