class TasksController < ApplicationController
  before_action :doorkeeper_authorize!

  def index
    @tasks = Task.all
    render json: @tasks
  end

  def create
    @task = Task.new(task_params)
    @task.save
    render json: @task
  end

  def update
    @task = Task.find(params[:id])
    @task.update(task_params)
    render json: @task
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy
    head :no_content
  end

  def completed_status
    @task = Task.find(params[:id])
    @task.completed = params[:completed]
    @task.save
    render json: @task
  end

  private

  def task_params
    params.require(:task).permit(:text)
  end
end
