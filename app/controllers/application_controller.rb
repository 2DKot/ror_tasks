class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end
end
