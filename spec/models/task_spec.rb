require 'rails_helper'

RSpec.describe Task, type: :model do
  context 'default sorting' do
    let!(:task1) { FactoryBot.create :task }
    let!(:task2) { FactoryBot.create :task }
    let!(:task3) { FactoryBot.create :task }

    it 'is chronologically' do
      expect(Task.all).to eq([task1, task2, task3])
    end
  end
end
