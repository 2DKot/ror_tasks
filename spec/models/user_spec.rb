require 'rails_helper'

RSpec.describe User, type: :model do
  context 'json seralizer' do
    let(:user) { FactoryBot.create :user }

    it 'serializes correctly' do
      expect(user.as_json).to eq(
        id: user.id,
        email: user.email,
        admin: user.admin
      )
    end
  end
end
