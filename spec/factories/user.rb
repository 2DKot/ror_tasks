FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.org" }
    password 'secret'
  end
end
