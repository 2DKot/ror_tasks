require 'rails_helper'

RSpec.describe 'Tasks resource with valid token' do
  let!(:user) { FactoryBot.create :user }
  let!(:token) { FactoryBot.create :access_token, resource_owner_id: user.id }
  let!(:headers) { { 'Authorization' => "Bearer #{token.token}" } }

  describe 'GET /tasks', type: :request do
    context 'when there are no tasks' do
      before do
        get '/tasks', headers: headers
      end

      it 'returns empty array' do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq('application/json')
        parsed_body = JSON.parse(response.body)
        expect(parsed_body).to eq([])
      end
    end

    context 'when there is one task' do
      let!(:task) { FactoryBot.create :task, text: 'new super task' }

      before do
        get '/tasks', headers: headers
      end

      it 'returns array with one task' do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq('application/json')
        parsed_body = JSON.parse(response.body)
        expect(parsed_body).to eq([{
          'id' => task.id,
          'text' => 'new super task',
          'url' => "/tasks/#{task.id}",
          'completed' => false,
          'completed_url' => "/tasks/#{task.id}/completed_status"
        }])
      end
    end
  end

  describe 'POST /tasks', type: :request do
    context 'with correct body' do
      before do
        post '/tasks', params: { task: { text: 'AAAA' } }, headers: headers
      end

      it 'creates task' do
        expect(response).to have_http_status(:success)
        @task = Task.last
        expect(@task.text).to eq('AAAA')
      end
    end
  end

  describe 'DELETE /tasks/1', type: :request do
    context 'when task exists' do
      let(:task) { FactoryBot.create :task }

      before do
        delete "/tasks/#{task.id}", headers: headers
      end

      it 'deletes task' do
        expect(response).to have_http_status(:success)
        expect { Task.find task.id }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'PUT /tasks/1', type: :request do
    context 'with correct body' do
      let(:original_task) { FactoryBot.create :task }
      let(:updated_task) { Task.last }

      before do
        put "/tasks/#{original_task.id}", params: { task: { text: 'AAAA' } }, headers: headers
      end

      it 'updates task' do
        expect(response).to have_http_status(:success)
        expect(original_task.text).not_to eq(updated_task.text)
        expect(updated_task.text).to eq('AAAA')
      end
    end
  end

  describe 'POST /tasks/1/completed_status', type: :request do
    context 'with completed false' do
      let(:original_task) { FactoryBot.create :task }
      let(:updated_task) { Task.last }

      before do
        post "/tasks/#{original_task.id}/completed_status", params: { completed: false }, headers: headers
      end

      it 'sets task.completed to false' do
        expect(response).to have_http_status(:success)
        expect(updated_task.completed).to eq(false)
      end
    end

    context 'with completed true' do
      let(:original_task) { FactoryBot.create :task }
      let(:updated_task) { Task.last }

      before do
        post "/tasks/#{original_task.id}/completed_status", params: { completed: true }, headers: headers
      end

      it 'sets task.completed to true' do
        expect(response).to have_http_status(:success)
        expect(updated_task.completed).to eq(true)
      end
    end
  end
end
