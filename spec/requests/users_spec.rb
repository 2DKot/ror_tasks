require 'rails_helper'

RSpec.describe 'Users resource with valid token' do
  describe 'GET /users/me', type: :request do
    let!(:user1) { FactoryBot.create :user }
    let!(:token1) { FactoryBot.create :access_token, resource_owner_id: user1.id }

    let!(:user2) { FactoryBot.create :user }
    let!(:token2) { FactoryBot.create :access_token, resource_owner_id: user2.id }

    context 'when authorized as first user' do
      before do
        get '/users/me', headers: { 'Authorization' => "Bearer #{token1.token}" }
      end

      it 'returns first user' do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq('application/json')
        parsed_body = JSON.parse(response.body).symbolize_keys
        expect(parsed_body).to eq(user1.as_json)
      end
    end

    context 'when authorized as second user' do
      before do
        get '/users/me', headers: { 'Authorization' => "Bearer #{token2.token}" }
      end

      it 'returns second user' do
        expect(response).to have_http_status(:success)
        expect(response.content_type).to eq('application/json')
        parsed_body = JSON.parse(response.body).symbolize_keys
        expect(parsed_body).to eq(user2.as_json)
      end
    end
  end
end
