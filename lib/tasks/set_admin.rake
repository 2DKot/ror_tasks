desc 'Set admin flag to user with specified email'
task :set_admin, [:email] => [:environment] do |_t, args|
  raise 'Needs email' if args.email.blank?
  @user = User.where(email: args.email).update(admin: true)
end
