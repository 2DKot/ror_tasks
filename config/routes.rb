Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }, skip: :sessions
  get '/users/me'

  use_doorkeeper

  root to: 'landing#index'

  resources :tasks, only: %i[index create update destroy] do
    member do
      post 'completed_status'
    end
  end
end
